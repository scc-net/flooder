#!/usr/bin/env python3
import argparse
import socket
import time
import re

class Arguments:
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Flood mac on interfaces.')

        parser.add_argument('-d', default=MAC(12*'f'), type=MAC, help='Destination MAC. Default is broadcast mac.')
        parser.add_argument('-I', type=float, help='Interval in seconds. Default is 1.')
        parser.add_argument('interface', nargs='*', help='Specify interfaces. Default is all interfaces except lo and interfaces starting wiht "dummy"')

        self.parser = parser

    @property
    def args(self):
        args = self.parser.parse_args()

        return args


class MAC:
    def __init__(self, mac):
        if isinstance(mac, str):
            mac = mac.lower().strip()
            mac = re.sub(r'(.*?)([0-9a-f]+|$|^)', lambda m: (''*len(m.groups()[0])+m.groups()[1]), mac)
            if len(mac) != 12:
                 raise ValueError('Not a mac: {}'.format(mac))
            self._mac = bytes.fromhex(mac)
        elif isinstance(mac, bytes):
            if len(mac)  != 6:
                 raise ValueError('Not a mac: {}'.format(mac.hex()))
            self._mac = mac
        elif isinstance(mac, MAC):
            self._mac = bytes(mac)
        else:
            ValueError('Not a mac')

    def __bytes__(self):
        return self._mac

class Packet:
    def __init__(self, src, dst=None):
        self._src = src
        if dst is None:
            self._dst = MAC(12*'f')
        else:
            self._dst = MAC(dst)

    @property
    def src(self):
        return self._src

    @property
    def dst(self):
        return self._dst


    def __bytes__(self):
        return bytes(self.dst) + bytes(self.src) + b'\x00\x00' + 46*b'\x00'


class Interface:
    def __init__(self, name):
        self._name = name
        self._mac = None
        self._socket = None

    def __repr__(self):
        return '{} {}'.format(self.name, self._mac)

    @property
    def socket(self):
        if self._socket is None:
            self._socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
            self._socket.bind((self.name, 0))
        return self._socket

    @property
    def name(self):
        return self._name

    @property
    def mac(self):
        if self._mac is None:
            self._mac = self.socket.getsockname()[-1]
        return self._mac

    def send(self, data):
        self._socket.send(bytes(data))

class Main:
    def __init__(self, interfaces = None, dst = None, interval = None):
        if not len(interfaces):
            self._interfaces = None
        else:
            self._interfaces = interfaces
        if dst is None:
            self._dst = MAC(12*'f')
        else:
            self._dst = MAC(dst)
        if interval is None:
            self._interval = 1.0
        else:
            self._interval = interval

    def run(self):
        while True:
            if self._interfaces == None:
                interfaces = [i[-1] for i in socket.if_nameindex() if i[-1] != 'lo' and not i[-1].startswith('dummy')]
            else:
                interfaces = self._interfaces
            for interface in map(Interface, interfaces):
                try:
                    packet = Packet(interface.mac, self._dst)
                    interface.send(packet)
                except OSError:
                    pass
            time.sleep(self._interval)

if __name__ == '__main__':
    args = Arguments().args
    main = Main(args.interface, args.d, args.I)
    main.run()
